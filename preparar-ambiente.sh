#!/bin/bash
MYUID=`id -u 2> /dev/null`
if [ ! -z "$MYUID" ]; then
    if [ $MYUID != 0 ]; then
       echo "You need root privileges to run this script.";
       exit 2
    fi
    else
	    echo "Could not detect UID";
            exit 2
fi

IDEAURL=http://download.jetbrains.com/idea/ideaIU-13.1.2.tar.gz
IDEAHome=/opt/jetbrains

cat <<EOF
Instalacao de ambiente de desenvolvimento Java:
* Autor: Giovanni Silva *
*** Use por sua conta e risco ***
EOF
echo "Configurando PPA's Aperte ENTER"
add-apt-repository ppa:webupd8team/java
apt-add-repository ppa:hachre/dart
apt-get update
echo "Instalando Java 8"
apt-get install oracle-java8-installer
echo "Instalando Compass e Ruby"
apt-get install ruby-full
gem install sass
gem install compass
echo "Instalando NodeJS e Karma"
apt-get install nodejs
apt-get install npm
apt-get install nodejs-legacy
npm install -g karma
npm install -g karma-dart
echo "Instalando bower e grunt-cli. Necessarios para fundation"
npm install -g bower grunt-cli
ln -s /usr/local/lib/node_modules/karma/bin/karma /usr/bin/karma
echo "Instalando Dart"
apt-get install dartsdk dartium
echo "Instalando Postgresql"
apt-get install postgresql
echo "Instalando GIT"
apt-get install git
echo "Instalando Intellij IDEA Ultimate"
mkdir -p $IDEAHome
wget -O /tmp/idea.tar.gz $IDEAURL
tar zxvf /tmp/idea.tar.gz -C /tmp
rm $IDEAHome/idea -rf
mv /tmp/idea-IU* $IDEAHome/idea
rm /tmp/idea.tar.gz

cat <<EOF
Todos os programas instalados.
Rode o IntellijIDEA com o comando /opt/jetbrains/idea/bin.sh Voce pode criar um atalho que funciona no menu do ubuntu apos abrir o IDE
em TOOLS > CREATE DESKTOP ENTRY
EOF
ls $IDEAHome
